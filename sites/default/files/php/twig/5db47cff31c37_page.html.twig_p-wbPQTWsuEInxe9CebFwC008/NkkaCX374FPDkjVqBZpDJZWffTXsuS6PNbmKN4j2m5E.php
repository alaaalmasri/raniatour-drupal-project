<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/raniatour/page.html.twig */
class __TwigTemplate_55e708247dce502aaaae765e980db97f3d1ce0b93b1164125d9854ef84e2e295 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 1, "if" => 5];
        $filters = ["escape" => 9];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/header.html.twig"), "themes/raniatour/page.html.twig", 1)->display($context);
        // line 2
        echo "<div id=\"main-content\">
    <div class=\"container\">
        <div class=\"row\">
            ";
        // line 5
        if ($this->getAttribute(($context["page"] ?? null), "related_programs", [])) {
            // line 6
            echo "            <div class=\"content-area col-md-8\">
                <section id=\"content\">
                    <div id=\"content-wrap\">
                        ";
            // line 9
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                    </div>
                </section>
            </div>
            <aside id=\"sidebar\" class=\"col-md-4\">
                ";
            // line 14
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "related_programs", [])), "html", null, true);
            echo "
            </aside>
            ";
        } else {
            // line 17
            echo "            <div class=\"content-area col-md-12\">
                <section id=\"content\">
                    <div id=\"content-wrap\">
                        ";
            // line 20
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                        ";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "promotions", [])), "html", null, true);
            echo "
                           ";
            // line 22
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "jordansites", [])), "html", null, true);
            echo "
                           ";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "booking", [])), "html", null, true);
            echo "
                    </div>
                </section>
            </div>

            ";
        }
        // line 29
        echo "
           

        </div>
    </div>
</div>
";
        // line 35
        $this->loadTemplate((($context["directory"] ?? null) . "/footer.html.twig"), "themes/raniatour/page.html.twig", 35)->display($context);
    }

    public function getTemplateName()
    {
        return "themes/raniatour/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 35,  109 => 29,  100 => 23,  96 => 22,  92 => 21,  88 => 20,  83 => 17,  77 => 14,  69 => 9,  64 => 6,  62 => 5,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/raniatour/page.html.twig", "C:\\wamp64\\www\\raniatour4\\themes\\raniatour\\page.html.twig");
    }
}
