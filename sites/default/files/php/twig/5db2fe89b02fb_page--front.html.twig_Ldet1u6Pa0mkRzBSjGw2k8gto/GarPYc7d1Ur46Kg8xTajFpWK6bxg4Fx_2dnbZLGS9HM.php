<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/raniatour/page--front.html.twig */
class __TwigTemplate_6c8c1a253346841a29c34363a01cc633f8865244c68027146a5edfc7b236da56 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 1];
        $filters = ["escape" => 4];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/header.html.twig"), "themes/raniatour/page--front.html.twig", 1)->display($context);
        // line 2
        echo "
<div class=\"slider\">
    ";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
        echo "
</div>
<div class=\"form\">
    <img src=\"themes/raniatour/assets/images/form.png\" class=\"img-responsive\">
</div>
<section class=\"promotion\">
    <div class=\"container\">
        <div class=\"promotion-title\">
            <h1>TOP TRAVEL DEALS AND PROMOTIONS</h1>
        </div>
        <div class=\"travel-deals\">
            ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "travel", [])), "html", null, true);
        echo "

        </div>

    </div>
    <!--end of travel deals-->
    <hr class=\"horzintal\">
</section><!-- end of section-->
<section class=\"Arrival\">
    <div class=\"container\">
        ";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "arrival", [])), "html", null, true);
        echo "

    </div>
</section>
<div class=\"section-1 box\"></div>
<div class=\"events\">
    <div class=\"container\">
        ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "experiance", [])), "html", null, true);
        echo "
    </div>
    <div class=\"events-box\">
        <div class=\"container\">
            ";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "trips", [])), "html", null, true);
        echo "
        </div>
    </div>
</div>
<div class=\"section-2 box\">
    <div class=\"news-blogs\">
        ";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "blogs", [])), "html", null, true);
        echo "
        <div class=\"container\">

            ";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "blogs2", [])), "html", null, true);
        echo "
        </div>

        <div class=\"compeny-brand\">
            <div class=\"container\">
                <div class=\"brand\">
               ";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "brand", [])), "html", null, true);
        echo "
                </div>
            </div>
        </div>
   
         ";
        // line 56
        $this->loadTemplate((($context["directory"] ?? null) . "/footer.html.twig"), "themes/raniatour/page--front.html.twig", 56)->display($context);
        // line 57
        echo "    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "themes/raniatour/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 57,  137 => 56,  129 => 51,  120 => 45,  114 => 42,  105 => 36,  98 => 32,  88 => 25,  75 => 15,  61 => 4,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/raniatour/page--front.html.twig", "C:\\wamp64\\www\\raniatour4\\themes\\raniatour\\page--front.html.twig");
    }
}
