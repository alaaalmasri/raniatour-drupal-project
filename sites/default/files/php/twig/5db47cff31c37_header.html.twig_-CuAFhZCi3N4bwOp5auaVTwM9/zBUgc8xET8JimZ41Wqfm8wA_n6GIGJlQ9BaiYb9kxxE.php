<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/raniatour/header.html.twig */
class __TwigTemplate_a05edf0f194dd6182c6e1f3be891171e5e7eb5c7afdb50b214e828ceebc813cd extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 12];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
  <header>
      <!--start of the header-->
      <div class=\"rania-top-header\">
          <!--start of rania top-header-->
          <div class=\"container\">
              <!-- start of container-->
              <div class=row>
                  <!--start of row-->
                  <div class=\"col-md-7 col-sm-4 col-xs-12\">
                      <!-- email and phone number-->
                      ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "contact", [])), "html", null, true);
        echo "
                  </div>
                  <!--end of the div that holds the email and phone number-->

                  <!--end of the empty block-->
                  <div class=\"col-md-5 col-sm-3 col-xs-12\">
                      <div class=\"social-media\">
                          ";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "social", [])), "html", null, true);
        echo "
                      </div>

                      <!--end of soical media div-->
                  </div>
                  <!--end of col-md-4-->
              </div>
              <!--end of col-md-4-->
          </div>
      </div>
      <!--end of header-->
      <div class=\"main-header\">

          <div class=\"row\">
              <div class=\"col-md-3 col-sm-4 col-xs-12\">
                  <img src=\"themes/raniatour/assets/images/logo.png\" class=\"img-responsive\">

              </div>
              <div class=\"col-md-7 col-sm-7 col-xs-12\">
                  <div class=\"logo-text\">
                      <p>Rania Tours is your new vision on tourism set </p>
                      <div class=\"rania-navbar\">
                          <!-- start of the navigation-->
                          <nav class=\"navbar navbar-expand-lg navbar-light \">
                              <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                                  <span class=\"navbar-toggler-icon\"></span>
                              </button>
                              <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                                  <ul class=\"navbar-nav mr-auto\">
                                      ";
        // line 48
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_nav", [])), "html", null, true);
        echo "

                                  </ul>
                              </div>
                          </nav>
                      </div>
                  </div>

              </div>
              <div class=\"col-md-2 col-sm-7 col-xs-12\">
                  <div class=\"login\" style=\"padding-top:70px;\">
                      <p>LOGIN<a href=\"\"><i style=\"margin-left:20px;  ; font-size: 15px;   color:#77787b; \" class=\"fa fa-search\"></i></a></p>
                  </div>
              </div>

          </div>
      </div>
  </header>
";
    }

    public function getTemplateName()
    {
        return "themes/raniatour/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 48,  78 => 19,  68 => 12,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/raniatour/header.html.twig", "C:\\wamp64\\www\\raniatour4\\themes\\raniatour\\header.html.twig");
    }
}
